var sharedServicesModule = angular.module('mSharedServices', []);
var loginModule = angular.module('mLogin', ['mSharedServices']);
var headerModule = angular.module('mHeader', ['mSharedServices', 'mLogin']);

var appModule = angular.module('mApp', ['mSharedServices', 'mLogin', 'mHeader']);
