loginModule.service('LoginService', function(NetworkService) {
	this.isLoggedIn = function () {
		return false;
	};
	
	this.getSessionDisplayName = function() {
		if (! this.isLoggedIn()) {
			return null;
		}
		
		return "Sanjay";
	};
});