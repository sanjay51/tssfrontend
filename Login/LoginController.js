loginModule.controller('LoginController', function ($scope, LoginService) {
	$scope.isLoggedIn = LoginService.isLoggedIn();
	$scope.displayName = LoginService.getSessionDisplayName();
});