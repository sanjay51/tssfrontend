loginModule.directive('loginBox', function() {
    return {
        restrict: 'E',
        scope: {
            info: '='
        },
        controller: "LoginController",
        templateUrl:'Login/_login.html'
    };
});
