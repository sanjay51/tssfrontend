headerModule.directive('headerBox', function() {
    return {
        restrict: 'E',
        scope: {
            info: '='
        },
        controller: "HeaderController",
        templateUrl:'Header/_header.html'
    };
});
